'''
Created on 27/10/2020

@author: rafael
'''
from tkinter.constants import NO
class Nodo:
    def __init__(self,dato=None):
        self.dato=dato
        self.nodoSiguiente=None
    def getdato(self):
        return self.dato
    def getnodoSiuiente(self):
        return self.nodoSiguiente
    def setDato(self,dato):
        self.dato=dato
    def setNodoSiguiente(self,siguiente):
        self.nodoSiguiente=siguiente
    def __str__(self):
        return f"Nodo dato= {self.dato} Nodo siguiente {self.nodoSiguiente}"
    
    
class ListaEnlazada():

    def __init__(self):
        self.nodoInicio = Nodo()
        self.nodoFinal= Nodo()
        self.nodoInicio.nodoSiguiente= self.nodoFinal
        self.numeroElementos=0
    def verificarVacia(self):
        if(self.numeroElementos==0):
            return True
        else:
            return False
    def vaciarLista(self):
        self.nodoInicio=self.nodoFinal=None
        self.numeroElementos=0
    def conexion(self,conectorA,receptor,conectorB):
        conectorA.nodoSiguiente=receptor
        receptor.nodoSiguiente=conectorB
        self.numeroElementos=self.numeroElementos+1
    def insertarDatoInicio(self,dato):
        self.conexion(self.nodoInicio,Nodo(dato),self.nodoInicio.nodoSiguiente)
    def insertarDatoFinal(self,dato):
        auxiliar=self.nodoInicio
        while auxiliar.nodoSiguiente!= self.nodoFinal:
            auxiliar=auxiliar.nodoSiguiente
        
        self.conexion(auxiliar,Nodo(dato),auxiliar.nodoSiguiente)

    def mostrarLista(self):
        if self.verificarVacia()==False:
            temporal=self.nodoInicio.nodoSiguiente
            print("Lista de elementos:")
            while temporal!=self.nodoFinal:
                print(f"[{temporal.dato}]--> ", end="")
                temporal=temporal.nodoSiguiente
            
            print()
        else:
            print("La lista esta vacia")
    
    def eliminarInicio(self):
        if(self.verificarVacia()==False):
            self.nodoInicio.nodoSiguiente=self.nodoInicio.nodoSiguiente.nodoSiguiente
            self.numeroElementos=self.numeroElementos-1
        else:
            print("No se puede eliminar ya que la lista esta vacia")
            
    def eliminarFinal(self):
        if(self.verificarVacia()==False):
            elemento=self.nodoInicio
            temporal=None
            while elemento.nodoSiguiente!=self.nodoFinal:
                temporal=elemento
                elemento=elemento.nodoSiguiente
            temporal.nodoSiguiente=self.nodoFinal
            self.numeroElementos=self.numeroElementos-1
        else:
            print("No se puede eliminar ya que la lista esta vacia")
    def elimiarDatoEspecifico(self,dato):
        if(self.verificarVacia()==False):
            datoEliminado=None
            
            if(dato==self.nodoInicio.nodoSiguiente.getdato()):
                
                datoEliminado=self.nodoInicio.nodoSiguiente.getdato()
                self.nodoInicio=self.nodoInicio.nodoSiguiente.nodoSiguiente
                self.numeroElementos=self.numeroElementos-1
                return datoEliminado
            
            else:
                
                anterior=None
                sucesor=self.nodoInicio.nodoSiguiente
                while sucesor!=self.nodoFinal:
                    
                    if(dato==sucesor.dato):
                        datoEliminado=sucesor
                        anterior.nodoSiguiente=sucesor.nodoSiguiente
                        return datoEliminado
                    
                    anterior=sucesor
                    sucesor=sucesor.nodoSiguiente
                    
                print("Dato no encontrado")
        else:
            print("La lista esta vacia")
            return None
            
                
                    
                
        pass
    
    def mostrarCantidadElementos(self):
        print(f"La cantidad de elementos es de: {self.numeroElementos}")
    
    def buscarElemento(self,dato):
        if(self.verificarVacia()==False):
            existe=False
            temporal=self.nodoInicio
            while temporal!=self.nodoFinal:
                temporal=temporal.nodoSiguiente
                if(temporal.dato==dato):
                    existe=True
            
            if existe:
                print(f"El dato: {dato} SI existe en la lista")
            else:
                print(f"El dato: {dato} NO existe en la lista")
        
        
        
        
    
    
        
    
    
#------------------------------------------------------

op=""
lista =ListaEnlazada()
while(op!="8"):
    print("1- Verificar si la lista esta vacia")
    print("2- Insertar elemento")
    print("3- Eliminar elemento")
    print("4- Mostrar Elementos")
    print("5- Vaciar lista")
    print("6- Mostrar Cantidad de elementos")
    print("7- Buscar elemetno")
    print("8- Salir")
    op=input()
    if(op=="1"):
        if(lista.verificarVacia()):
            print("La lista esta vacia")
        else:
            print("La lista no esta vacia")
    elif(op=="2"):
        op2=""
        dato=int(input("Ingresa el dato que deseas insertar: "))
        while(not(op2=="1" or op2=="2") ):
            print("1- Agregar al principio de la fila el dato")
            print("2- Agregar al final de la fila el dato")
            op2=input()
            if(op2=="1"):
                lista.insertarDatoInicio(dato)
            elif(op2=="2"):
                lista.insertarDatoFinal(dato)
            else:
                print("Ingresa una opcion disponible")
    elif(op=="3"):
        op3=""
        while(not(op3=="1" or op3=="2" or op3=="3") ):
            print("1- Eliminar el primer Elemento")
            print("2- Eliminar el ultimo elemento")
            print("3- Eliminar dato especifico")
            op3=input()
            if(op3=="1"):
                lista.eliminarInicio()
            elif(op3=="2"):
                lista.eliminarFinal()
            elif(op3=="3"):
                seElimino=lista.elimiarDatoEspecifico(int(input("Ingresa el dato que deseas eliminar: ")))
                if(not seElimino==None):
                    print("Se elimino el dato correctamente")
            else:
                print("Ingresa una opcion disponible")
    elif(op=="4"):
        lista.mostrarLista()
    elif(op=="5"):
        lista.vaciarLista()
    elif(op=="6"):
        lista.mostrarCantidadElementos()
    elif(op=="7"):
        lista.buscarElemento(int(input("Ingresa el dato que deseas buscar: ")))
    elif(op=="8"):
        print("Saliendo....")
    else:
        print("Elige una opcion correcta")


    